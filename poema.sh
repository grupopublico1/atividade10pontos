#!/bin/bash

vermei='\033[0;31m'
verde='\033[0;32m'

echo -e "${vermei}No meio do caminho tinha uma pedra"
sleep 0.5
echo -e "${verde}Tinha uma pedra no meio do caminho"
sleep 0.5
echo -e "${vermei}Tinha uma pedra"
sleep 0.5
echo -e "${verde}No meio do caminho tinha uma pedra."
sleep 0.5
echo -e "${vermei}Nunca me esquecerei desse acontecimento"
sleep 0.5
echo -e "${verde}Na vida de minhas retinas tão fatigadas."
sleep 0.5
echo -e "${vermei}Nunca me esquecerei que no meio do caminho"
sleep 0.5
echo -e "${verde}Tinha uma pedra"
sleep 0.5
echo -e "${vermei}Tinha uma pedra no meio do caminho"
sleep 0.5
echo -e "${verde}No meio do caminho tinha uma pedra."
sleep 0.5

